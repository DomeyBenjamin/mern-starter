const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const app = express();
const path = require('path');
const passport = require('passport');

// Connect to MongoDB
mongoose
  .connect(
    'mongodb://localhost:27017/mern',
    { useNewUrlParser: true }
  )
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err));
//require models
require('./App/Model/user');

// Takes the raw requests and turns them into usable properties on req.body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//validates all requests
app.use(expressValidator());

// serves up static files from the public folder. Anything in public/ will just be served up as the file it is
app.use(express.static(path.join(__dirname, 'public')));

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());


//routes
app.use('/api', require('./routes/api'));
app.use('/auth', require('./routes/auth'));
const port = process.env.NODE_ENV || 5000;


app.listen(port, () => `Server running on port ${port}`);