import React, { Component } from 'react';
import './App.css';
import {
  Home,
  Login,
  Register
} from './Screens/index';
import CustomHeader from './components/Header';
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';
import {Provider} from 'react-redux'
import store from './store/store';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div>
            <CustomHeader />
            <Route path="/" exact component={Home} />
            <Route path="/login" exact component={Login} />
            <Route path="/register" exact component={Register} />
          </div>
        </Router>
      </Provider>
      
      
    );
  }
}

export default App;
