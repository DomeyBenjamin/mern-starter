const User = (state = null, action) => {
    switch(action.type){
        case 'AUTHENTICATE_USER':
            return {
                name:action.payload.name,
                email:action.payload.email
            };
        case 'LOGOUT':
                return null;
        default:
            return state;
    }
}

export default User;