import React, { Component } from 'react';
import '../App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  Nav,
  Navbar,
} from 'react-bootstrap';
import {
    Link
  } from 'react-router-dom';
  import {connect} from 'react-redux';

class App extends Component {
  render() {
    return (
        <div className="App">
          <Navbar bg="light" expand="lg">
              <Navbar.Brand href="/">Mern</Navbar.Brand>
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                  <Nav.Link>
                  <Nav.Link href="">
                        <Link to="/" style={{textDecoration:'none'}}>Home</Link>
                      </Nav.Link>
                  </Nav.Link>
                </Nav>
                {
                  (this.props.UserDetail === null)&&(
                    <Nav>
                      <Nav.Link href="">
                        <Link to="/login" style={{textDecoration:'none'}}>login</Link>
                      </Nav.Link>
                      <Nav.Link href="">
                        <Link to="/register" style={{textDecoration:'none'}}>Register</Link>
                      </Nav.Link>
                    </Nav>
                  )
                }
                
              </Navbar.Collapse>
            </Navbar>
        </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    UserDetail:state
  }
}

export default connect(mapStateToProps)(App);
