import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
    Container,
    Row,
    Form,
    Button,
    Alert,
    Spinner
} from 'react-bootstrap';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';


class Login extends Component {
    constructor(props){
        super(props);
        this.state = {
            validated:false,
            email:"",
            password:"",
            errorMsg:"",
            redirect:false,
            isSuccessful:false
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    
    }

    componentDidMount(){
        if(this.props.UserDetail !== null){
            this.setState({
                redirect:true
            })
        }
    }
    
    handleChange = (event, item) => {
        this.setState({
            [item]: event.target.value
        })
    }

    handleSubmit = (event) => {
        const form = event.currentTarget;
        const { email, password } = this.state;
        if(form.checkValidity() === false){
            event.preventDefault();
            event.stopPropagation();
            this.setState({
                validated:true
            })
        }else{
            event.preventDefault();
            this.setState({
                validated:true,
                isSuccessful:true,
                errorMsg:""
            })
            axios.post('/api/users/login',{
                email,
                password
            },{
                headers:{
                    'Accept':'application/json',
                    'Content-Type':'application/json'
                }
            })
            .then(user => {
                if(user.error){
                    this.setState({
                        errorMsg:user.data.errorMessage,
                        isSuccessful:false
                    });
                }else{
                    this.props.AUTHENTICATE_USER_SAVE(user.data);
                    this.setState({
                        redirect:true
                    });
                }
            })
            .catch(err => {
                console.log(err);
            });
        }
        
        
    }
  render() {
    if(this.state.redirect){
        return <Redirect to="/" />
    }
    return (
      <Container className="mt-5" >
      <Row className="d-flex justify-content-end">
            {
                (this.state.errorMsg.trim() !== "")&&(
                    <Alert variant="danger">
                        {this.state.errorMsg}
                    </Alert>
                )
            }
            
        </Row>
        <Row className="d-flex justify-content-center">
            <h3 className="text h1 text-info">Login</h3>
        </Row>
        <Row className="justify-content-center align-items-center mt-1 flex-direction-row">
            <Form noValidate validated={this.state.validated} onSubmit={this.handleSubmit}>
                <Form.Group controlId="forEmail">
                    <Form.Control value={this.state.email} onChange={(event)=>this.handleChange(event, 'email')} size="lg" type="email" required placeholder="Enter your email" />
                    <Form.Control.Feedback type="invalid">Email is required!</Form.Control.Feedback>
                </Form.Group>
                <Form.Group controlId="forPassword">
                    <Form.Control value={this.state.password} onChange={(event) => this.handleChange(event, 'password')} size="lg" type="password" required placeholder="Enter your password" />
                    <Form.Control.Feedback type="invalid">Password is required!</Form.Control.Feedback>
                </Form.Group>
                <Form.Group controlId="forRememberMe">
                    <Form.Check size="lg" type="checkbox" label="Remember me" />
                </Form.Group>
                {
                    (this.state.isSuccessful) && (
                        <Button size="lg" variant="primary" block disabled>
                            <Spinner
                            as="span"
                            animation="grow"
                            size="sm"
                            role="status"
                            aria-hidden="true"
                            />
                            Loading...
                        </Button>     
                    )
                }
                {
                    (!this.state.isSuccessful)&&(
                        <Button block size="lg" type="submit" variant="primary">
                            Submit
                        </Button>
                    )
                }
            </Form>
        </Row>        
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => {
    return {
        AUTHENTICATE_USER_SAVE:(user) => dispatch({
            type:'AUTHENTICATE_USER',
            payload:user.data
        })
    }
}


const mapStateToProps = state => {
    return {
      UserDetail:state
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
