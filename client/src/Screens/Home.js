import React, { Component } from 'react';
import logo from '../logo.svg';
import '../App.css';
import Customers from '../components/customers';
import { Redirect } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import {connect} from 'react-redux';

class Home extends Component {
  constructor(props){
    super(props);
    this.state = {
      authRedirect:false
    }
  }

  componentDidMount(){
    if(this.props.UserDetail === null){
      this.setState({
        authRedirect:true
      })
    }
  }

  render() {
    if(this.state.authRedirect){
      return <Redirect to="/login" />
    }
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">React Express Starter</h1>
        </header>
       
        <Customers />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    UserDetail:state
  }
}

export default connect(mapStateToProps)(Home);
