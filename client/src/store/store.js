import { createStore } from 'redux';
import User from '../reducers/user';
const store = createStore(User);
export default store;