const express = require('express');
const Router = express.Router();
const HomeController = require('../App/Https/Controller/HomeController');
const AuthController = require('../App/Https/Controller/AuthController');

Router.get('/customers', HomeController.index);
Router.post('/users/login',AuthController.login);
Router.post('/users/register',AuthController.register);

module.exports = Router;