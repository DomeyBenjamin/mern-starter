const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    trim: true,
    required:true
  },
  email: {
    type: String,
    trim: true,
    required:true,
    unique:true
  },
  password: {
    type: String,
    required:true
  },
  resetPasswordToken: String,
  resetPasswordExpires:Date,
  created_at: {
    type: Date,
    default: Date.now
  },
  updated_at:{
    type:Date,
    default:Date.now
  }
});

const User = mongoose.model('User', UserSchema);

module.exports = User;
