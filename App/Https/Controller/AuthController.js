const bycrypt = require('bcryptjs');
const mongoose = require('mongoose');
const User = mongoose.model('User');



exports.login = (req, res)=>{
    User.findOne({email: req.body.email},(err, user)=>{
        if(err){
            res.json({error: true, errorMessage:"The provided email does not exist in our records"});
        }

        if(user){
            bycrypt.compare(req.body.password, user.password,(err, isMatch)=>{
                if (err) throw err;
                if(isMatch){
                    res.json({
                        error: false,
                        data: user
                    });
                }else {
                    res.json({
                        error: true,
                        data: null,
                        errorMessage: "Sorry wrong email/password"
                    });
                }
            })
        }

    });
}


exports.register = (req, res) =>{
    User.findOne({email: req.body.email})
    .then(user => {
        if(user){
            res.json({error: true, errorMessage:"Email already exists"});
        }else{
            const newUser = new User({
                name:req.body.name,
                email:req.body.email,
                password:req.body.password
            });

            bycrypt.genSalt(10, (err, salt)=>{
                bycrypt.hash(newUser.password,salt,(err, hash)=>{
                    // if(err) throw err;
                    newUser.password = hash;
                    newUser.save().then(user =>{
                        res.json({error: false, data: user});
                    })
                })
            })
        }
    })    
}

